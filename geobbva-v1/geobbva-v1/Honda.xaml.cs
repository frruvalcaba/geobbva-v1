﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Honda : ContentPage
    {
        public Honda()
        {
            InitializeComponent();
        }

        private void WebBBVA_Clicked(object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.bbva.mx/", BrowserLaunchMode.SystemPreferred);
        }
    }
}
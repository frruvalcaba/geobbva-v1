﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Liverpool : ContentPage
    {
        public Liverpool()
        {
            InitializeComponent();
        }

        private void WebLiv_Clicked(object sender, EventArgs e)
        {
            Browser.OpenAsync("https://www.liverpool.com.mx/tienda/home", BrowserLaunchMode.SystemPreferred);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FirstContact : ContentPage
    {
        Boolean stLiverpool = false;
        Boolean stSears = false;
        Boolean stSanborns = false;
        Boolean stHonda = false;
        Boolean stNissan = false;
        Boolean stPalacio = false;
        Boolean stBestBuy = false;
        Boolean stAeroMexico = false;
        Boolean stVolaris = false;
        Boolean stUber = false;
        Boolean stOxxo = false;

        public FirstContact()
        {
            InitializeComponent();
            
        }

        private void cbLiverpool_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                stLiverpool = true;
                test.Text = "Liverpool";
            }
            else
            {
                stLiverpool=false;
                test.Text = "";
            }
        }

        private void cbSears_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Sears";
                stSears = true;
            }
            else
            {
                test.Text = "";
                stSears = false;
            }
        }

        private void cbSanborns_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Sanborns";
                stSanborns = true;
            }
            else
            {
                test.Text = "";
                stSanborns = false;
            }
        }

        private void cbHonda_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Honda";
                stHonda = true;
            }
            else
            {
                test.Text = "";
                stHonda = false;
            }
        }

        private void cbNissan_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Nissan";
                stNissan = true;
            }
            else
            {
                test.Text = "";
                stNissan = false;
            }
        }

        private void cbPalacioDeHierro_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Palacio de Hierro";
                stPalacio = true;
            }
            else
            {
                test.Text = "";
                stPalacio = false;
            }
        }

        private void cbBestBuy_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "BestBuy";
                stBestBuy = true;
            }
            else
            {
                test.Text = "";
                stBestBuy = false;
            }
        }

        private void cbAeroMexico_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "AeroMeixico";
                stAeroMexico = true;
            }
            else
            {
                test.Text = "";
                stAeroMexico = false;
            }
        }

        private void cbVolaris_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Volaris";
                stVolaris = true;
            }
            else
            {
                test.Text = "";
                stVolaris = false;
            }
        }

        private void cbUber_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Uber";
                stUber = true;
            }
            else
            {
                test.Text = "";
                stUber = false;
            }
        }

        private void cbOxxo_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (e.Value == true)
            {
                test.Text = "Oxxo";
                stOxxo = true;
            }
            else
            {
                test.Text = "";
                stOxxo = false;
            }
        }

        private async void btnRegistrar_Clicked_1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Catalogo(stLiverpool, stSears, stSanborns, stHonda, stNissan, stPalacio, stBestBuy, stAeroMexico, stVolaris, stUber, stOxxo));
        }
    }
}
﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace geobbva_v1.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private AmazonDynamoDBClient client;
        private ObservableCollection<TestVM> customers;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowViewModel()
        {
            try
            {
                var credentials = new BasicAWSCredentials("AKIAIBO256XAEU2X5GUA", "5GYoBECRpiFbKC9jTRhZ3tI6zqvMbsvYTYPpMY8L");
                this.client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USEast2);
                this.customers = new ObservableCollection<TestVM>();
                //this.CreateTestVMsTable();
                this.AddCustomers();
                this.LoadData(null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: failed to create a DynamoDB client; " + ex.Message);
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public ObservableCollection<TestVM> TestVMs
        {
            get { return this.customers; }
            set
            {
                if (this.customers != value)
                {
                    this.customers = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private async void CreateTestVMsTable()
        {
            List<string> currentTables = client.ListTablesAsync().Result.TableNames;

            if (!currentTables.Contains("Customers"))
            {
                CreateTableRequest createRequest = new CreateTableRequest
                {
                    TableName = "Customers",
                    AttributeDefinitions = new List<AttributeDefinition>()
            {
                new AttributeDefinition
                {
                    AttributeName = "Id",
                    AttributeType = "N"
                },
                new AttributeDefinition
                {
                    AttributeName = "Name",
                    AttributeType = "S"
                }
            },
                    KeySchema = new List<KeySchemaElement>()
            {
                new KeySchemaElement
                {
                    AttributeName = "Id",
                    KeyType = "HASH"
                },
                new KeySchemaElement
                {
                    AttributeName = "Name",
                    KeyType = "RANGE"
                }
            },
                };

                createRequest.ProvisionedThroughput = new ProvisionedThroughput(1, 1);

                CreateTableResponse createResponse;
                try
                {
                    createResponse = await client.CreateTableAsync(createRequest);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
        }
        private async void AddCustomers()
        {
            var table = Table.LoadTable(client, "Customers");

            var obj1 = new Document();
            obj1["id"] = "2";
            obj1["name"] = "Ethel";
            obj1["employees"] = 446;
            obj1["state"] = "NY";

            await table.PutItemAsync(obj1);
            //if (search.Count == 0)
            //{
            //    Document dataObj1 = new Document();
            //    dataObj1["name"] = "Telerik";
            //    dataObj1["id"] = 2;
            //    dataObj1["employees"] = 446;
            //    dataObj1["state"] = "NY";
            //    await table.PutItemAsync(dataObj1);

            //    Document dataObj2 = new Document();
            //    dataObj2["name"] = "Progress";
            //    dataObj2["id"] = 13;
            //    dataObj2["employees"] = 1054;
            //    dataObj2["State"] = "IL";
            //    await table.PutItemAsync(dataObj2);

            //    Document dataObj3 = new Document();
            //    dataObj3["name"] = "NativeScript Inc.";
            //    dataObj3["id"] = 31;
            //    dataObj3["employees"] = 109;
            //    dataObj3["state"] = "WAS";
            //    await table.PutItemAsync(dataObj3);
            //}
        }
        private async void LoadData(object obj)
        {
            var table = Table.LoadTable(client, "Customers");
            var search = table.Scan(new Expression());

            var documentList = new List<Document>();
            do
            {
                documentList.AddRange(await search.GetNextSetAsync());

            } while (!search.IsDone);

            var customers = new ObservableCollection<TestVM>();
            foreach (var doc in documentList)
            {
                var customer = new TestVM();
                foreach (var attribute in doc.GetAttributeNames())
                {
                    var value = doc[attribute];
                    if (attribute == "id")
                    {
                        customer.Id = Convert.ToInt32(value.AsPrimitive().Value);
                    }
                    else if (attribute == "name")
                    {
                        customer.Name = value.AsPrimitive().Value.ToString();
                    }
                    else if (attribute == "employees")
                    {
                        customer.Employees = Convert.ToInt32(value.AsPrimitive().Value);
                    }
                    else if (attribute == "state")
                    {
                        customer.State = value.AsPrimitive().Value.ToString();
                    }
                }

                customers.Add(customer);
            }

            this.customers = customers;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace geobbva_v1.Tables
{
    class RegUserTable
    {
        public Guid UserId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Fecha_de_nacimiento { get; set; }
        public string Telefono { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }




    }
}

﻿using geobbva_v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Layout : ContentPage
    {
        
            public Layout()
            {
                InitializeComponent();
                 this.BindingContext = new MainWindowViewModel();
            }
    }
}
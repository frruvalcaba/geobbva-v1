﻿using geobbva_v1.Tables;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void btcancelar_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void btcontinuar_Clicked(object sender, EventArgs e)
        {
            var dbpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "UserDatabase.db");
            var db = new SQLiteConnection(dbpath);
            db.CreateTable<RegUserTable>();

            var item = new RegUserTable()
            {

                Nombre = Nombre.Text,
                Apellido = Apellido.Text,
                Email = Email.Text,
                Fecha_de_nacimiento = (string)BirthDate.BindingContext,
                Telefono = Telefono.Text,
                Pais = Pais.Text,
                Estado = Estado.Text,
                Municipio = Municipio.Text

            };
            db.Insert(item);
            Device.BeginInvokeOnMainThread(async () =>
            {
                var result = await this.DisplayAlert("Felicidades", "Su usuario ha sido registrado", "Ok", "Cancelar");


            });
              Navigation.PushAsync(new FirstContact());

        }

        private void BirthDate_DateSelected(object sender, DateChangedEventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace geobbva_v1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Catalogo : ContentPage
    {
        public Catalogo(Boolean statLiv, Boolean statSears, Boolean statSanborns, Boolean statHonda, Boolean statNissan, Boolean statPalacio, Boolean statBest, Boolean statAeromexico, Boolean statVolaris, Boolean statUber, Boolean statOxxo)
        {
            InitializeComponent();
            if (statLiv == true)
            {
                btnLivOfertas.IsVisible = true;
            }
            else
            {
                btnLivOfertas.IsVisible = false;
            }

            if (statSears == true)
            {
                btnSearsOfertas.IsVisible = true;
            }
            else
            {
                btnSearsOfertas.IsVisible = false;
            }

            if (statSanborns == true)
            {
                btnSanbornsOfertas.IsVisible = true;
            }
            else
            {
                btnSanbornsOfertas.IsVisible = false;
            }

            if (statHonda == true)
            {
                btnHondaOfertas.IsVisible = true;
            }
            else
            {
                btnHondaOfertas.IsVisible = false;
            }

            if (statNissan == true)
            {
                btnNissanOfertas.IsVisible = true;
            }
            else
            {
                btnNissanOfertas.IsVisible = false;
            }

            if (statPalacio == true)
            {
                btnPalacioOfertas.IsVisible = true;
            }
            else
            {
                btnPalacioOfertas.IsVisible = false;
            }

            if (statBest == true)
            {
                btnBestBuyOfertas.IsVisible = true;
            }
            else
            {
                btnBestBuyOfertas.IsVisible = false;
            }

            if (statAeromexico == true)
            {
                btnAeromexicoOfertas.IsVisible = true;
            }
            else
            {
                btnAeromexicoOfertas.IsVisible = false;
            }

            if (statVolaris == true)
            {
                btnVolarisOfertas.IsVisible = true;
            }
            else
            {
                btnVolarisOfertas.IsVisible = false;
            }

            if (statUber == true)
            {
                btnUberOfertas.IsVisible = true;
            }
            else
            {
                btnUberOfertas.IsVisible = false;
            }

            if (statOxxo == true)
            {
                btnOxxoOfertas.IsVisible = true;
            }
            else
            {
                btnOxxoOfertas.IsVisible = false;
            }

        }

        private async void btnLivOfertas_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Liverpool());
        }

        private async void btnHondaOfertas_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Honda());
        }

        private async void btnVolarisOfertas_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Volaris());
        }
    }
}